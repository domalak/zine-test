var mongoose = require('mongoose')
require('./models/person')
var Person = mongoose.model('Person')

var express = require('express')
var app = express()

mongoose.Promise = global.Promise
var mongoURI = process.env.MONGODB_URI ? process.env.MONGODB_URI : 'mongodb://localhost/zine-test'
mongoose.connect(mongoURI)
var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', main)

app.get('/api/person', getPerson)

app.use('/', express.static('client'))

function main() {
  app.listen(process.env.PORT || 3000, function () {
    console.log('App started')
  })
}

function getPerson(req, res) {
  Person.find()
  .then(function (persons) {
    res.json(persons)
  })
}
