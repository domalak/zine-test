var mongoose = require('mongoose')
require('../models/person')
var Person = mongoose.model('Person')

var firstNames = [
  'Alice',
  'Mike',
  'John',
  'Lisa',
  'Maria',
  'Bradley',
  'Veronica',
  'Matt',
  'Angelina',
  'Victoria',
  'William'
]

var lastNames = [
  'Aniston',
  'Pitt',
  'Johnson',
  'Bush',
  'Forbes',
  'Skywalker',
  'Cage',
  'Jobs',
  'Gates',
  'Zuckerberg',
  'Smith'
]

var mailDomains = [
  '@gmail.com',
  '@yahoo.com',
  '@outlook.com',
  ''
]

var locations = [
  'London',
  'Paris',
  'Amsterdam',
  'New York',
  'Los Angeles',
  'Berlin',
  'Milan',
  ''
]

var followers = [
  [0, 0],
  [10, 100],
  [100, 1000],
  [1000, 10000],
  [10000, 100000],
  [100000, 1000000]
]

mongoose.Promise = global.Promise
var mongoURI = process.env.MONGODB_URI ? process.env.MONGODB_URI : 'mongodb://localhost/zine-test'
mongoose.connect(mongoURI)
var db = mongoose.connection
db.once('open', main)

function main() {
  Person.remove()
  .then(fillWithData)
}

function fillWithData() {
  var i = 0
  firstNames.forEach(function (firstName) {
    lastNames.forEach(function (lastName) {
      var email = mailDomains[getRandomInt(0, mailDomains.length)]
      email = email ? firstName + '.' + lastName + email : ''
      var location = locations[getRandomInt(0, locations.length)]
      var instagram = getRandomFollowers()
      var blog = getRandomFollowers()
      var facebook = getRandomFollowers()
      var twitter = getRandomFollowers()
      var youtube = getRandomFollowers()
      var pinterest = getRandomFollowers()

      Person.create({
        name: firstName + ' ' + lastName,
        email: email.toLowerCase(),
        location: location,
        instagram: instagram,
        blog: blog,
        facebook: facebook,
        twitter: twitter,
        youtube: youtube,
        pinterest: pinterest,
        snapchat: getRandomInt(0, 2)
      })
      .then(function () {
        i++
        if (i >= firstNames.length * lastNames.length) {
          console.log('done')
          process.exit()
        }
      })
    })
  })
}

function getRandomFollowers() {
  var range = followers[getRandomInt(0, followers.length)]
  return getRandomInt(range[0], range[1])
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}
