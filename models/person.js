var mongoose = require('mongoose')

var personSchema = mongoose.Schema({
  name: String,
  email: String,
  instagram: Number,
  blog: Number,
  facebook: Number,
  twitter: Number,
  youtube: Number,
  snapchat: Boolean,
  pinterest: Number,
  location: String
})

mongoose.model('Person', personSchema)
