angular.module('Zine')
.filter('zineNumber', function() {
  return function(input) {
    if (!input) {
      return 0;
    }

    if (input < 1000) {
      return input
    }

    var out = Math.floor(input / 100);

    if (out % 10 === 0 || input > 99999) {
      return Math.floor(out / 10) + 'k'
    }

    return Math.floor(out / 10) + '.' + (out % 10) + 'k'
  };
});
