function socialDataController($http, $filter) {
  var sdCtrl = this
  var persons

  $http.get('/api/person')
  .then(function (res) {
    // console.log(res);
    persons = res.data;
    sdCtrl.filter()
  })

  sdCtrl.orderBy = function (column) {
    if (sdCtrl.orderColumn === '+' + column) {
      return sdCtrl.orderColumn = '-' + column
    }

    sdCtrl.orderColumn = '+' + column
  }

  sdCtrl.filter = function () {
    sdCtrl.persons = $filter('filter')(persons, function (person) {
      for (var key in sdCtrl.filterOptions) {
        if (sdCtrl.filterOptions[key] && person[key] < sdCtrl.filterOptions[key]) {
          return false
        }
      }

      return true
    })
  }

  sdCtrl.selectAll = function () {
    var value = sdCtrl.allSelected() ? false : true

    sdCtrl.persons.forEach(function (person) {
      person.selected = value
    })
  }

  sdCtrl.allSelected = function () {
    return sdCtrl.persons && sdCtrl.persons.every(function (person) {
      return person.selected
    })
  }
}

angular.module('Zine')
.component('zineSocialData', {
  templateUrl: 'app/components/social-data/social-data.html',
  controllerAs: 'sdCtrl',
  controller: socialDataController
});
